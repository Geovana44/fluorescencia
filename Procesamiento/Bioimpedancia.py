import numpy as np
import math
import scipy.signal
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import entropy
from scipy import integrate
from pathlib import Path
from io import BytesIO
import base64
import pdb
import os
from django.conf import *

import mpld3

def leerarchivo(Muestra_bio):
    #path = os.path.join(os.path.dirname(os.path.realpath(__file__)), archivo)
    base_dir =settings.MEDIA_ROOT 
    archivo_bio = Muestra_bio
    my_file = os.path.join(base_dir, archivo_bio) 
    
    ####Lee el archivo  y lo almacena en una matriz    
    M_bio = np.loadtxt(my_file, delimiter=',', usecols=range(2), skiprows=2)
    frecuencia = M_bio[:,0] ###Extraer columna 1 de los datos
    fase = M_bio[:,1]##extraer columna 2 de los datos
    fig = plt.figure()
    graficabio = plt.plot(frecuencia, fase)
    base_dir =settings.MEDIA_ROOT    
    n=1
    NomImagbio = os.path.join(base_dir, 'biompedancia/imagenes/imagen')
    plt.savefig(NomImagbio +str(n) +".jpg")
    #breakpoint()
    ####maximo 
    MAXfase= np.max(fase)
    MINfase= np.min(fase) 
    MINposi= np.argmin(fase) 
    
    frecuenciaWo=M_bio[MINposi][0]
    frecuenciamedia = np.abs(np.min(M_bio[:,0])-np.max(M_bio[:,0]))/2
    ####desvacion estandar fase
    Destandar = np.std(fase)
    #### Promedio 
    Promedio = np.mean(fase)
    print(MAXfase, MINfase, frecuenciaWo, frecuenciamedia, Destandar, Promedio )

    return M_bio, NomImagbio


