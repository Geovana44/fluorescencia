from django.urls import path
from django.urls import include
from apps.termography.views import Itermo
from . import views


urlpatterns = [
    path('Itermo', Itermo, name='Intertermo'),
    ]